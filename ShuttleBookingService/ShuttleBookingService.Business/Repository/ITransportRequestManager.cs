﻿using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public interface ITransportRequestManager
    {
        List<TransportRequest> GetAllTransportRequests();
        TransportRequest AddTransportRequest(TransportRequest entity);
    }
}
