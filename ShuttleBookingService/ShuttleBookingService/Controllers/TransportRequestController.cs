﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShuttleBookingService.Business.Repository;
using ShuttleBookingService.Model.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ShuttleBookingService.Controllers
{
    [Route("api/[controller]")]
    public class TransportRequestController : ControllerBase
    {

        private readonly ITransportRequestManager _transportRequestManager;
        public TransportRequestController(ITransportRequestManager TransportRequestManager)
        {
            _transportRequestManager = TransportRequestManager;
        }
        // GET: api/<controller>
        [HttpGet]
        public IActionResult GellAll()
        {
            var transportRequests = _transportRequestManager.GetAllTransportRequests();
            var apiResponse = new ApiResponse();
            apiResponse.message = "Find all transport requests";
            apiResponse.status = "Success";
            apiResponse.data = transportRequests;
            return Ok(apiResponse);
            //return Ok(new TokenResponseModel { Status = "ok", Message = "valid token" });
            //return CreateResponse(HttpStatusCode.Unauthorized, businessUsers);
            //return new HttpResponseMessage(HttpStatusCode.OK, new { transportRequests });
            //return Json(new { Result = result, Id = ID });
            // return Ok(message,status,transportRequests);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
        [HttpPost]
        public ActionResult AddTransportRequest([FromBody]TransportRequest requests)
        {
           var requestId = _transportRequestManager.AddTransportRequest(requests);
            var apiResponse = new ApiResponse();
            apiResponse.message = "Shuttle book request is submitted successfully !!";
            apiResponse.status = "Success";
            apiResponse.data = requestId.request_id;
            return Ok(apiResponse);
        }
        //// POST api/<controller>
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
