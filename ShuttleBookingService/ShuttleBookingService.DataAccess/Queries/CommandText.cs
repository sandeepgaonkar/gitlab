﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.DataAccess.Queries
{
    public class CommandText : ICommandText
    {
       
        public string GetAllTransportRequests => "select * from public.\"transport_request\"";
        public string AddTransportRequest => "INSERT INTO public.\"transport_request\"(employee_id, first_name, last_name, user_name, sex, contact_phone, project_account, email, request_reason, pickup_location_id, to_location, to_lattitude, to_longitude, pin_code, no_of_passengers, status, request_time, created_on, cancelled_on, vendor_id, approved_by, approved_on, approved_by_dm, approved_by_dm_on, approval_reason, total_expense)" +
            "VALUES(@employee_id, @first_name, @last_name, @user_name, @sex, @contact_phone, @project_account, @email, @request_reason, @pickup_location_id, @to_location, @to_lattitude, @to_longitude, @pin_code, @no_of_passengers, @status, @request_time, @created_on, @cancelled_on, @vendor_id, @approved_by, @approved_on, @approved_by_dm, @approved_by_dm_on, @approval_reason, @total_expense) RETURNING request_id";
    }
}
